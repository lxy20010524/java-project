import java.util.Scanner;
import java.util.StringTokenizer;

public class CreateTree {
    public LinkedBinaryTree<String> CreatTree(String inorder[],String preorder[]){
        LinkedBinaryTree<String> binaryTree = null;
        if(inorder.length == preorder.length && inorder.length != 0 && preorder.length != 0){
            int n = 0;
            //根据前历第序遍一个root 进行中序遍历
            while (!inorder[n].equals(preorder[0])) {
                n++;
            }
            //根据前序遍历第一个root 进行中序遍历
            //左子树的数组输入
            String[] preLeft = new String[n];
            String[] inLeft = new String[n];
            //右子树的数组输入
            String[] preRight = new String[preorder.length - n - 1];
            String[] inRight = new String[inorder.length - n - 1];
            for (int t = 0; t < inorder.length; t++) {
                if (t < n) {
                    preLeft[t] = preorder[t + 1];//左子树生成
                    inLeft[t] = inorder[t];//左子树生成
                }
                if (t > n) {
                    preRight[t - n - 1] = preorder[t];//右子树生成
                    inRight[t - n - 1] = inorder[t];//右子树生成
                }
                if(t == n){//
                    continue;
                }
            }
       /* //重新生成树
        LinkedBinaryTree<String> left = CreatTree(inLeft, preLeft);
        LinkedBinaryTree<String> right = CreatTree(inRight, preRight);
        binaryTree = new LinkedBinaryTree<String>(preorder[0], left, right);//树的输出
        //   System.out.println(binaryTree.toString());*/
            LinkedBinaryTree<String> left = CreatTree(inLeft, preLeft);
            LinkedBinaryTree<String> right = CreatTree(inRight, preRight);
            binaryTree = new LinkedBinaryTree<String>(preorder[0], left, right);//树的输出
            //   System.out.println(binaryTree.toString());
        }else


        //若不满足以下条件则无法构成一棵树。无法生成树
        {
            binaryTree = new LinkedBinaryTree<>();
        }
        return binaryTree;
    }

    public static void main(String[] args) throws ArrayIndexOutOfBoundsException
    {
        String a,b;
        int i = 0,j = 0;
        Scanner scanner  = new Scanner(System.in);
        System.out.println("Input the PreOrder：");
        a = scanner.nextLine();
        System.out.println("Input the PostOrder：");
        b = scanner.nextLine();
        //分别用" "分割，取出每个字符
        StringTokenizer str1 = new StringTokenizer(a, " ");
        StringTokenizer  str2= new StringTokenizer(b, " ");
        //数组
        String[] string1 = new String[str1.countTokens()];
        String[] string2 = new String[str2.countTokens()];
        //放入数组
        while (str1.hasMoreTokens())
        {
            string1[i] = str1.nextToken();
            i++;
        }


        while (str2.hasMoreTokens())
        {
            string2[j] = str2.nextToken();
            j++;
        }



        //中序输出
        //前序输出
        CreateTree ct = new CreateTree();
        LinkedBinaryTree<String> binaryTree = ct.CreatTree(string1,string2);
        System.out.println("Tree:");
        System.out.println();
        System.out.println(binaryTree.toString());
    }
}