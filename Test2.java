import java.util.Stack;

public class Test2 {
    public static void main(String[] args) {

        int []test=new int[10];
        test[0]=1;
        test[1]=10;
        test[2]=1;
        test[3]=8;
        test[4]=8;
        test[5]=6;
        test[6]=2;
        test[7]=3;
        test[8]=0;
        test[9]=4;

        NewSearching searching=new NewSearching();
        System.out.println("线性查找： ");
        searching.linear(test, 1);
        NewSorting sorting=new NewSorting();
        sorting.selectionSort(test);
        searching.binarySearch(test, 2);
        System.out.println("二分查找： ");
        System.out.println("找到目标数的下标："+searching.binSearch(test, 0, test.length-1, 1));
        System.out.println("插值查找： ");
        System.out.println("找到目标数的下标："+searching.insertSearch(test, 0, test.length-1, 1));
        System.out.println("斐波那契查找： ");
        System.out.println("找到目标数的下标："+ searching.fibSearch(test,3));

        /*Sorting sorting=new Sorting();*/
        System.out.println("希尔排序： ");
        sorting.shellSort(test);
        /*Sorting sorting=new Sorting();*/
        System.out.println("直接插入排序： ");
        sorting.insertSort(test);
    }
}