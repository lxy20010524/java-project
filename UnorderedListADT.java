public interface UnorderedListADT<T> extends ListADT
{
    public void addToFront(T element);

    public void addToRear(T element);

    public void addAfter(T element, T target);
}