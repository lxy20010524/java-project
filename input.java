public class input {
    Node head=null;
    Node temp=null;

    public void addNode(int d){
        Node N=new Node(d);
        if (head==null)
        {
            head=N;
            return;
        }
        temp=head;
        while (temp.next!=null)
        {
            temp=temp.next;
        }
        temp.next=N;
    }
}
