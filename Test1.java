package search;

import search.Contact;
import search.Searching;
import search.Sorting;

public class Test1 {
    public static void main(String[] args) {
        Contact[] a = new Contact[5];
        Contact[] b = new Contact[5];

        a[0] = new Contact("a", "b", "2303");
        a[1] = new Contact("c", "d", "2020");
        a[2] = new Contact("e", "f", "2000");
        a[3] = new Contact("g", "h", "1999");
        a[4] = new Contact("i", "j", "2023");
        b[0] = new Contact("k", "l", "123");
        b[1] = new Contact("m", "n", "234");
        b[2] = new Contact("o", "p", "345");
        b[3] = new Contact("q", "r", "456");
        b[4] = new Contact("s", "t", "567");
        Contact target1 = new Contact("", "", "123");//小边界
        Contact target2 = new Contact("", "", "2303");//大边界
        Contact target3 = new Contact("", "", "2023");//正常
        Contact target4 = new Contact("", "", "234");//正常
        Contact target5 = new Contact("", "", "345");//正常
        Contact target6 = new Contact("", "", "0010");//异常
        Contact target7 = new Contact("", "", "999");//异常


        Contact found[] = new Contact[10];
        found[0] = (Contact) Searching.linearSearch(b, target1);
        found[1] = (Contact) Searching.linearSearch(a, target2);
        found[2] = (Contact) Searching.linearSearch(a, target3);
        found[3] = (Contact) Searching.linearSearch(b, target4);
        found[4] = (Contact) Searching.linearSearch(b, target5);
        found[5] = (Contact) Searching.linearSearch(b, target6);
        found[6] = (Contact) Searching.linearSearch(a, target7);
        found[7] = (Contact) Searching.linearSearch(b, target7);
        found[8] = (Contact) Searching.linearSearch(a, target6);
        found[9] = (Contact) Searching.linearSearch(a, target1);
        for (int i = 1; i <= 10; i++) {
            System.out.println("Test" + i + ":");
            if (found[i - 1] == null)
                System.out.println("Can't found it!");
            else
                System.out.println("Found:  " + found[i - 1]);
        }

        Sorting.selectionSort(a);//倒序
        Sorting.selectionSort(b);//正序

        System.out.println("Test11:");
        for (Comparable play : a)
            System.out.println(play);
        System.out.println("Test12:");
        for (Comparable play : b)
            System.out.println(play);
    }
}
