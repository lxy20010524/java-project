import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UnGraph {
    public List<String> verticesSize; //顶点个数
    public int[][] edges;//邻接矩阵
    public Boolean[] Visit;//遍历；

    public UnGraph(int x) {
        edges = new int[x][x];
        verticesSize = new ArrayList<>(x);
        Visit = new Boolean[x];//初始化邻接矩阵

        for (int i = 0; i < x; i++) {
            Visit[i] = false;
        }
        for (int j = 0; j < x; j++) {
            verticesSize.add(j, "" + j);
        }
        int a, b;
        for (a = 0; a < x; a++) {
            for (b = 0; b < x; b++) {
                edges[a][b] = 0;
            }
        }
    }

    public void displayGraph(){
        for(int []x:edges){
            System.out.println(Arrays.toString(x));
        }
    }

    public void AddEdgeWeight(int x1,int x2,int w){
        edges[x1][x2]=w;
        edges[x2][x1]=w;
    }

    public void Prim(int n){
        int min = 100;
        int pls = 0 ;
        int count = 0;
        int i;
        for(i=0;i<verticesSize.size();i++){

            if(Visit[i]){
                count ++;
            }
        }
        Visit[n]=true;
        if(count == verticesSize.size()){
            System.out.println("out!");
        }else {
            for(i=0;i<verticesSize.size();i++){
                if((min >=edges[n][i])&&(edges[n][i]!=0)&&(!Visit[i]&&(n<=i))){
                    min = edges[n][i];
                    pls = i;
                }
            }
            if (min!=100){
                System.out.println(n+"--"+pls);
            }
            Prim(pls);
        }
    }
}
