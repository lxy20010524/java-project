public class ⅠTeacher extends ⅠHuman {
    public ⅠTeacher(String name){
        super(name);
    }
    public void greet() {
        System.out.println("02 here is teacher " + this.name);
    }
    public void work(){
        greet();
        System.out.println("and my work is teaching");
    }
}
