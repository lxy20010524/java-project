import java.util.Arrays;

public abstract class ArrayStack<T> implements StackADT<T>{
    T [] arrayStack2023;
    int top = 0;
    int count = 0;
    public ArrayStack(T[] arrayStack2023) {
        this.arrayStack2023 = arrayStack2023;

    }

    @Override
    public void push(T element) {
        if (top != arrayStack2023.length){
            arrayStack2023[top++] = element;
            count++;
        }else {
            System.out.println("the stack is full");
            T [] newArray = (T[])(new Object[arrayStack2023.length*2]);
            this.arrayStack2023 = newArray;
        }
    }

    @Override
    public T pop() {
        if (isEmpty() != false) {
            System.out.println("full");
        }
        top--;
        return arrayStack2023[top];
    }

    @Override
    public T peek() {
        if(count <= 0){
            return null;
        }
        return arrayStack2023[top];
    }

    @Override
    public boolean isEmpty() {
        if(count == 0){
            return true;
        }else{
        return false;
        }
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public String toString() {
        return "ArrayStack{" +
                "arrayStack2023=" + Arrays.toString(arrayStack2023) +
                ", top=" + top +
                '}';
    }
}
