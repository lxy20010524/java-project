import java.sql.Array;
import java.util.ArrayList;

public class ArrayListTest {
    public static void main(String[] args) {
        ArrayList list = new ArrayList();

        //1.实现功能“增加元素”
        list.add("element1");
        list.add("element2");
        list.add("element3");
        System.out.println(list);

        //2.实现功能“删除元素”
        list.remove(2);
        list.remove(0);
        System.out.println(list);
        //修复被删除的元素
        list.clear();
        list.add("element1");
        list.add("element2");
        list.add("element3");

        //3.实现功能“修改元素”
        list.set(0,"不是element1");
        System.out.println(list);

        //4.实现功能“索引元素”
        System.out.println("第1个元素是："+list.get(0));

        //5.实现功能“判断集合是否为空”
        boolean empty = list.isEmpty();
        System.out.println(empty);

        //6.实现功能“返回list长度”
        System.out.println(list.size());



    }
}
