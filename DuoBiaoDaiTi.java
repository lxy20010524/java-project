import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;
public class DuoBiaoDaiTi {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList arrayList = new ArrayList();
        System.out.println("请输入需要加密的文件：");
        String name = scanner.next();
        String path = ("d:\\"+name+".txt");
        File file = new File(path);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String termString = null;
        while (true){
            try {
                if ((termString = reader.readLine()) == null) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            arrayList.add(termString);
        }
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //System.out.println("请输入需要加密的明文：");
        //String newlight = scanner.nextLine();
        String newlight = arrayList.toString();
        String x = "abcdefghijklmnopqrstuvwxyz";
        char [] list0 = new char[26];
        for(int i =0;i<26;i++){
            list0[i]=x.charAt(i);
        }
        System.out.println("请输入密钥字：");
        String str = scanner.next();
        int n = str.length();//记录密钥字长度
        char [] miyao = new char[n];
        for(int i =0;i<n;i++){
            miyao[i]= str.charAt(i);
        }
        //表一，使用密钥字加密
        char [] list1  = new char[26];
        System.arraycopy(miyao, 0, list1, 0, n);
        int cout=0;
        int k=0;
        for(int i=0;i<26;i++){
            for(int j=0;j<n;j++){
                if((int)list0[i]==(int)miyao[j]){
                    cout++;
                }
            }if(cout==0){
                list1[k+n]=list0[i];
                k++;
            }else {
                cout=0;
            }
        }
        //表二，使用洗牌法
        char [] list2 = new char[26];
        for(int i=0;i<26;i++){
            list2[i] = x.charAt(i);
        }
        //利用随机交换进行洗牌
        char temp;
        for(int i=0;i<26;i++) {
            int random=(int)(Math.random()*26);//0~25
            if((int)list2[i]<(int)list2[random]){
                temp=list2[i];
                list2[i] = list2[random];
                list2[random] = temp;
            }
        }
        //表三，公式法表，数学公式为：y=x;
        char [] list3 = new char[26];
        for(int i=0;i<26;i++){
            list3[i] = x.charAt(i);
        }
        //周期为3的多表代替密码
        //System.out.println("请输入明文：  （EN）");
        String light = newlight.replaceAll(" ","");
        int lightNum = light.length();

        char [] lightlist = new char[lightNum];
        for(int i=0;i<lightNum;i++){
            lightlist[i]=light.charAt(i);
        }
        int lightcout = 0;
        char [] darklist = new char[lightNum];
        for(int i=0;i<lightNum;i++){
            if((int)lightlist[i]>96&&(int)lightlist[i]<123){
                if(lightcout%3==0){
                    int a = lightlist[i];
                    darklist[i]=list1[a-97];
                    lightcout++;
                }else if(lightcout%3==1){
                    int b = lightlist[i];
                    darklist[i]=list2[b-97];
                    lightcout++;
                }else if(lightcout%3==2){
                    int c = lightlist[i];
                    darklist[i]=list3[c-97];
                    lightcout++;
                }
            }else if((int)lightlist[i]<96||(int)lightlist[i]>123){
                darklist[i]=lightlist[i];
            }
        }
        File file1 = new File("d:\\多表替换加密.txt");
        try {
            file1.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedWriter Information = null;
        try {
            Information = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file1), "Utf-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        for(int i=0;i<lightNum;i++){
            try {
                assert Information == null;
                Information.write(darklist[i]);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (Information != null) {
            try {
                Information.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
