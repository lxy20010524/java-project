import java.util.Scanner;

public class XuanZe {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("从键盘输入十个数");
        int []Array1 = new int[10];
        int []Array2 = new int[10];
        int []Array3 = new int[20];
        for(int i=0;i<Array1.length;i++){
            Array1[i]=scanner.nextInt();
            Array3[i]=Array1[i];
        }
        System.out.println("对Array1使用选择排序：");
        for(int i=0;i<Array1.length;++i){
            int min = i;
            for(int j = i+1;j<Array1.length;j++){
                if(Array1[j]<Array1[min]){
                    int temp = Array1[j];
                    Array1[j]=Array1[min];
                    Array1[min] = temp;
                }
            }
        }
        for (int j : Array1) {
            System.out.println(j + "");
        }
        System.out.println();

        System.out.println("从键盘输入十个数");

        for(int i=0;i<Array2.length;i++){
            int k = i+10;
            Array2[i]=scanner.nextInt();
            Array3[k]=Array2[i];
        }
        System.out.println("对Array2使用冒泡排序：");
        System.out.println();
        int temp;
        for(int i = 0;i<Array2.length;i++){
            for(int n = 0;n<Array2.length-1;n++){
                if(Array2[n]>Array2[n+1]){
                    temp = Array2[n];
                    Array2[n]=Array2[n+1];
                    Array2[n+1]=temp;
                }
            }
        }
        for (int j : Array2) {
            System.out.println(j);
        }

        System.out.println("使用插入排序合并数组");
        ChaRu in = new ChaRu();
        in.sort(Array3);

    }
    void sort(int date[]){
        for(int i=0;i<date.length;i++){
            for(int j=i;j>0&&(date[j]<date[j-1]);j--){
                int temp = date[j];
                date[j]=date[j-1];
                date[j-1]=temp;
            }
        }
        for(int i = 0 ;i<date.length;i++){
            System.out.println(date[i]);
        }
    }
}
