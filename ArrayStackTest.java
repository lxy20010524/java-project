import java.util.ArrayList;
import java.util.Stack;

public class ArrayStackTest {
    public static void main(String[] args) {
        Stack s = new Stack();


        s.push(10);
        s.push(20);
        s.push(30);
        s.push(40);
        s.push(50);
        System.out.println("s:"+s);

        s.peek();
        System.out.println("s:"+s);

        System.out.println(s.size());

        s.pop();
        System.out.println("s:"+s);

        System.out.println(s.isEmpty());

        System.out.println(s.size());

        System.out.println(s.toString());



    }
}
