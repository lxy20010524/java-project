import java.io.*;
import java.util.Scanner;
public class MainList {//结构体对象
    protected String name;
    protected int college;
    protected int item;
    protected float score;
    protected int rank;
    protected int points;
    protected int sum;
    public static void main(String[] args) {
        System.out.println("欢迎使用运动会信息统计系统");
        System.out.println("程序初始化，请组织者根据提示输入运动会相关数据");
        System.out.println();
        //确定学校数量，给予编号
        System.out.println("请输入参与运动会的学校的数量");
        Scanner scanner = new Scanner(System.in);
        int schoolnum = 0;
        while (true){
            if(schoolnum<1){
                schoolnum = scanner.nextInt();
            }else {
                break;
            }
        }
        System.out.println("请依次输入参会学校");
        String[] schoolname = new String[schoolnum];
        for (int i = 0; i < schoolnum; i++) {
            schoolname[i] = scanner.next();
        }
        for (int i = 0; i < schoolnum; i++) {
            System.out.println((i + 1) + "号为" + schoolname[i]);
        }
        //确定男子项目数量和女子项目数量
        System.out.println("请依次输入入选运动会男子项目数量和女子项目数量");
        int m = -1;//男子项目
        while (true){
            if(m<0){
                System.out.println("男子项目数：");
                m = scanner.nextInt();
            }else {
                break;
            }
        }
        int w = -1;//女子项目
        while (true){
            if(w<0){
                System.out.println("女子项目数：");
                w = scanner.nextInt();
            }else {
                break;
            }
        }
        String[] Item = new String[(m + w)];
        System.out.println("请依次输入男子项目和女子项目");
        for (int i = 0; i < (m + w); i++) {
            Item[i] = scanner.next();
        }
        for (int i = 0; i < (m + w); i++) {
            System.out.println((i + 1) + "号为" + Item[i]);
        }
        //输入运动员信息
        System.out.println("请输入运动员数目");
        int n = 0;
        while (true){
            if(n<1){
                n = scanner.nextInt();
            }else {
                break;
            }
        }
        MainList[][] players = new MainList[n][6];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 6; j++) {
                players[i][j] = new MainList();
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 4; ) {
                System.out.println("请输入"+(i+1)+"号运动员信息（姓名，学校编号，参赛项目编号，比赛成绩）");
                players[i][j].name = scanner.next();
                j++;
                players[i][j].college = scanner.nextInt();
                j++;
                players[i][j].item = scanner.nextInt();
                j++;
                players[i][j].score = scanner.nextFloat();
                j++;
            }
        }
        //先以项目编号排序，再对同一项目成绩排序，赋予运动员排名，赋分
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if (players[j][2].item < players[i][2].item) {

                    String name = players[j][0].name;
                    players[j][0].name = players[i][0].name;
                    players[i][0].name = name;

                    int college = players[j][1].college;
                    players[j][1].college = players[i][1].college;
                    players[i][1].college = college;

                    float score = players[j][3].score;
                    players[j][3].score = players[i][3].score;
                    players[i][3].score = score;

                    int item = players[j][2].item;
                    players[j][2].item = players[i][2].item;
                    players[i][2].item = item;
                }
            }
        }
        //组织者决定项目成绩排序方式
        System.out.println("决定比赛项目成绩排序方式和赋分方式");
        System.out.println();
        int sin = 0;
        for (int itemn = 1; itemn <= m + w; itemn++) {
            int cot = 0;
            for (int itemnum = 0; itemnum < n; itemnum++) {
                if (players[itemnum][2].item == itemn) {
                    cot++;
                }
            }
            System.out.println("请选择" + itemn + "号项目的排序方式(1从小到大，2从大到小)");
            int a = scanner.nextInt();
            if (a == 1 || a == 2) {
                if (a == 1) {
                    for (int i = sin; i < cot + sin; i++) {
                        for (int j = i + 1; j < cot + sin; j++) {
                            if (players[j][3].score < players[i][3].score) {
                                String name = players[j][0].name;
                                players[j][0].name = players[i][0].name;
                                players[i][0].name = name;

                                int college = players[j][1].college;
                                players[j][1].college = players[i][1].college;
                                players[i][1].college = college;

                                float score = players[j][3].score;
                                players[j][3].score = players[i][3].score;
                                players[i][3].score = score;

                                int item = players[j][2].item;
                                players[j][2].item = players[i][2].item;
                                players[i][2].item = item;
                            }
                        }
                    }
                    System.out.println("请选择赋分方式：1为前三名，2为前五名");
                    int k = scanner.nextInt();
                    if (k == 1 || k == 2) {
                        int ranknum = 1;
                        int samenum = 0;
                        players[sin][4].rank = ranknum;
                        if (k == 1) {
                            for (int i = sin + 1; i < sin + 3; i++) {
                                if (i < n) {
                                    if (players[i][3].score == players[i - 1][3].score) {
                                        players[i][4].rank = ranknum;
                                        samenum++;
                                    } else {
                                        players[i][4].rank = ranknum + 1 + samenum;
                                        ranknum++;
                                    }
                                } else {
                                    break;
                                }
                            }
                            for (int i = sin; i < sin + 3; i++) {
                                if (i < n) {
                                    if (players[i][4].rank == 1) {
                                        players[i][5].points = 5;
                                    } else if (players[i][4].rank == 2) {
                                        players[i][5].points = 3;
                                    } else if (players[i][4].rank == 3) {
                                        players[i][5].points = 2;
                                    } else {
                                        players[i][5].points = 0;
                                    }
                                }
                            }
                        } else {
                            for (int i = sin + 1; i < sin + 5; i++) {
                                if (i < n) {
                                    if (players[i][3].score == players[i - 1][3].score) {
                                        players[i][4].rank = ranknum;
                                        samenum++;
                                    } else {
                                        players[i][4].rank = ranknum + 1 + samenum;
                                        ranknum++;
                                    }
                                } else {
                                    break;
                                }
                            }
                            for (int i = sin; i < sin + 5; i++) {
                                if (i < n) {
                                    if (players[i][4].rank == 1) {
                                        players[i][5].points = 7;
                                    } else if (players[i][4].rank == 2) {
                                        players[i][5].points = 5;
                                    } else if (players[i][4].rank == 3) {
                                        players[i][5].points = 3;
                                    } else if (players[i][4].rank == 4) {
                                        players[i][5].points = 2;
                                    } else if (players[i][4].rank == 5) {
                                        players[i][5].points = 1;
                                    } else {
                                        players[i][5].points = 0;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    for (int i = sin; i < cot + sin; i++) {
                        for (int j = i + 1; j < cot + sin; j++) {
                            if (players[j][3].score > players[i][3].score) {
                                String name = players[j][0].name;
                                players[j][0].name = players[i][0].name;
                                players[i][0].name = name;

                                int college = players[j][1].college;
                                players[j][1].college = players[i][1].college;
                                players[i][1].college = college;

                                float score = players[j][3].score;
                                players[j][3].score = players[i][3].score;
                                players[i][3].score = score;

                                int item = players[j][2].item;
                                players[j][2].item = players[i][2].item;
                                players[i][2].item = item;
                            }
                        }
                    }
                    System.out.println("请选择赋分方式：1为前三名，2为前五名");
                    int k = scanner.nextInt();
                    if (k == 1 || k == 2) {
                        int ranknum = 1;
                        int samenum = 0;
                        players[sin][4].rank = ranknum;
                        if (k == 1) {
                            for (int i = sin + 1; i < sin + 3; i++) {
                                if (i < n) {
                                    if (players[i][3].score == players[i - 1][3].score) {
                                        players[i][4].rank = ranknum;
                                        samenum++;
                                    } else {
                                        players[i][4].rank = ranknum + 1 + samenum;
                                        ranknum++;
                                    }
                                } else {
                                    break;
                                }
                            }
                            for (int i = sin; i < sin + 3; i++) {
                                if (i < n) {
                                    if (players[i][4].rank == 1) {
                                        players[i][5].points = 5;
                                    } else if (players[i][4].rank == 2) {
                                        players[i][5].points = 3;
                                    } else if (players[i][4].rank == 3) {
                                        players[i][5].points = 2;
                                    } else {
                                        players[i][5].points = 0;
                                    }
                                }
                            }
                        } else {
                            for (int i = sin + 1; i < sin + 5; i++) {
                                if (i < n) {
                                    if (players[i][3].score == players[i - 1][3].score) {
                                        players[i][4].rank = ranknum;
                                        samenum++;
                                    } else {
                                        players[i][4].rank = ranknum + 1 + samenum;
                                        ranknum++;
                                    }
                                } else {
                                    break;
                                }
                            }
                            for (int i = sin; i < sin + 5; i++) {
                                if (i < n) {
                                    if (players[i][4].rank == 1) {
                                        players[i][5].points = 7;
                                    } else if (players[i][4].rank == 2) {
                                        players[i][5].points = 5;
                                    } else if (players[i][4].rank == 3) {
                                        players[i][5].points = 3;
                                    } else if (players[i][4].rank == 4) {
                                        players[i][5].points = 2;
                                    } else if (players[i][4].rank == 5) {
                                        players[i][5].points = 1;
                                    } else {
                                        players[i][5].points = 0;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            sin = sin + cot;
        }
        /*for (int i = 0; i < n; i++) {//检查初数组正确与否
            for (int j = 0; j < 6; ) {
                System.out.println(players[i][j].name);
                j++;
                for (int q = 0; q < schoolnum; q++) {
                    if (q + 1 == players[i][j].college) {
                        System.out.println(schoolname[q]);
                    }
                }
                j++;
                for (int q = 0; q < w + m; q++) {
                    if (q + 1 == players[i][j].item) {
                        System.out.println(Item[q]);
                    }
                }
                j++;
                System.out.println("成绩：" + players[i][j].score);
                j++;
                if (players[i][j].rank != 0) {
                    System.out.println("项目排名：" + players[i][j].rank);
                }
                j++;
                System.out.println("获得积分：" + players[i][j].points);
                j++;
            }
        }*/
        //将比赛信息存入文件
        File file = new File("d:\\information.txt");
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedWriter playerInformation = null;
        try {
            playerInformation = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "Utf-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 5; j++) {
                try {
                    assert playerInformation != null;
                    playerInformation.write("姓名："+players[i][j].name+"--");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                j++;
                for (int q = 0; q < schoolnum; q++) {
                    if (q + 1 == players[i][j].college) {
                        try {
                            playerInformation.write("学校："+schoolname[q]+"--");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                j++;
                for (int q = 0; q < w + m; q++) {
                    if (q + 1 == players[i][j].item) {
                        try {
                            playerInformation.write("项目："+Item[q]+"--");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                j++;
                try {
                    playerInformation.write("成绩："+players[i][j].score+"--");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                j++;
                try {
                    playerInformation.write("排名：" + players[i][j].rank+"--");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                j++;
                try {
                    playerInformation.write("积分：" + players[i][j].points+"");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                j++;
                try {
                    playerInformation.newLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (playerInformation != null) {
            try {
                playerInformation.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //根据用户需求查询信息
        while (true) {
            System.out.println("按1由学校总分排序输出");
            System.out.println("按2由学校编号排序输出");
            System.out.println("按3由男女团体总分排序输出");
            System.out.println("按4由学校编号查询某个项目信息");
            System.out.println("按5由项目编号查询学校排名");
            System.out.println("按0退出该查询系统");
            int choose = 1;
            do{
                if(choose<0&&choose>5){
                    System.out.println("非法输入，请重新选择");}
                choose = scanner.nextInt();
            }while (choose<0&&choose>5);
            if(choose==0){
                break;
            }
            switch (choose) {
                case 1:
                    MainList[][] schoolsumpoint = new MainList[schoolnum][2];
                    for (int i = 0; i < schoolnum; i++) {
                        for (int j = 0; j < 2; j++) {
                            schoolsumpoint[i][j] = new MainList();
                        }
                    }
                    for (int i = 0; i < schoolnum; i++) {
                        schoolsumpoint[i][0].name = schoolname[i];
                    }
                    for (int i = 0; i < schoolnum; i++) {
                        for (int j = 0; j < n; j++) {
                            if (players[j][1].college == i + 1) {
                                schoolsumpoint[i][1].sum = schoolsumpoint[i][1].sum + players[j][5].points;
                            }
                        }
                    }
                    for (int i = 0; i < schoolnum; i++) {
                        for (int j = i + 1; j < schoolnum; j++) {
                            if (schoolsumpoint[j][1].sum > schoolsumpoint[i][1].sum) {

                                String newname = schoolsumpoint[j][0].name;
                                schoolsumpoint[j][0].name = schoolsumpoint[i][0].name;
                                schoolsumpoint[i][0].name = newname;

                                int newpoint = schoolsumpoint[j][1].sum;
                                schoolsumpoint[j][1].sum = schoolsumpoint[i][1].sum;
                                schoolsumpoint[i][1].sum = newpoint;
                            }
                        }
                    }
                    for (int i = 0; i < schoolnum; i++) {
                        for (int j = 0; j < 2; ) {
                            System.out.println(schoolsumpoint[i][j].name);
                            j++;
                            System.out.println(schoolsumpoint[i][j].sum);
                            j++;
                        }
                    }break;
                case 2:
                    for (int i = 0; i < n; i++) {
                        for (int j = i + 1; j < n; j++) {
                            if (players[j][1].college < players[i][1].college) {

                                String name = players[j][0].name;
                                players[j][0].name = players[i][0].name;
                                players[i][0].name = name;

                                int college = players[j][1].college;
                                players[j][1].college = players[i][1].college;
                                players[i][1].college = college;

                                int item = players[j][2].item;
                                players[j][2].item = players[i][2].item;
                                players[i][2].item = item;

                                float score = players[j][3].score;
                                players[j][3].score = players[i][3].score;
                                players[i][3].score = score;

                                int rank = players[j][4].rank;
                                players[j][4].rank = players[i][4].rank;
                                players[i][4].rank = rank;

                                int points = players[j][5].points;
                                players[j][5].points = players[i][5].points;
                                players[i][5].points = points;
                            }
                        }
                    }
                    for (int i = 0; i < n; i++) {
                        for (int j = 0; j < 5; ) {
                            System.out.println(players[i][j].name);
                            j++;
                            for (int q = 0; q < schoolnum; q++) {
                                if (q + 1 == players[i][j].college) {
                                    System.out.println(schoolname[q]);
                                }
                            }
                            j++;
                            for (int q = 0; q < w + m; q++) {
                                if (q + 1 == players[i][j].item) {
                                    System.out.println(Item[q]);
                                }
                            }
                            j++;
                            System.out.println("成绩：" + players[i][j].score);
                            j++;
                            if (players[i][j].rank != 0) {
                                System.out.println("项目排名：" + players[i][j].rank);
                            }
                            j++;
                            System.out.println("获得积分：" + players[i][j].points);
                            j++;
                        }
                    }
                    break;
                case 3:
                    MainList[][] Itempoint = new MainList[2][2];
                    for (int i = 0; i < 2; i++) {
                        for (int j = 0; j < 2; j++) {
                            Itempoint[i][j] = new MainList();
                        }
                    }
                    Itempoint[0][0].name = "男子团体";
                    Itempoint[1][0].name = "女子团体";
                    for (int i = 0; i < n; i++) {
                        if (players[i][2].item <= m) {
                            Itempoint[0][1].sum = Itempoint[0][1].sum + players[i][5].points;
                        } else {
                            Itempoint[1][1].sum = Itempoint[1][1].sum + players[i][5].points;
                        }
                    }
                    System.out.println(Itempoint[0][0].name);
                    System.out.println(Itempoint[0][1].sum);
                    System.out.println(Itempoint[1][0].name);
                    System.out.println(Itempoint[1][1].sum);
                    break;
                case 4:
                    System.out.println("请选择想查找的学校");
                    for (int i = 0; i < schoolnum; i++) {
                        System.out.println((i + 1) + "号为" + schoolname[i]);
                    }
                    int a = scanner.nextInt();
                    System.out.println("请选择想要查找的项目");
                    for (int i = 0; i < (m + w); i++) {
                        System.out.println((i + 1) + "号为" + Item[i]);
                    }
                    int b = scanner.nextInt();
                    for (int i = 0; i < n; i++) {
                        if (players[i][1].college == a && players[i][2].item == b) {
                            for (int j = 0; j < 6; ) {
                                System.out.println(players[i][j].name);
                                j++;
                                for (int q = 0; q < schoolnum; q++) {
                                    if (q + 1 == players[i][j].college) {
                                        System.out.println(schoolname[q]);
                                    }
                                }
                                j++;
                                for (int q = 0; q < w + m; q++) {
                                    if (q + 1 == players[i][j].item) {
                                        System.out.println(Item[q]);
                                    }
                                }
                                j++;
                                System.out.println("成绩：" + players[i][j].score);
                                j++;
                                if (players[i][j].rank != 0) {
                                    System.out.println("项目排名：" + players[i][j].rank);
                                }
                                j++;
                                System.out.println("获得积分：" + players[i][j].points);
                                j++;
                            }
                        }
                    }
                    break;
                case 5:
                    MainList[][] CollegeItemRank = new MainList[schoolnum][2];
                    for (int i = 0; i < schoolnum; i++) {
                        for (int j = 0; j < 2; j++) {
                            CollegeItemRank[i][j] = new MainList();
                        }
                    }
                    for(int i=0;i<schoolnum;i++){
                        CollegeItemRank[i][0].name=schoolname[i];
                    }
                    System.out.println("请选择项目");
                    for (int i = 0; i < (m + w); i++) {
                        System.out.println((i + 1) + "号为" + Item[i]);
                    }
                    int c = scanner.nextInt();
                    for(int i=0;i<n;i++){
                        if(players[i][2].item==c){
                            for(int j=0;j<schoolnum;j++){
                                if(players[i][1].college==j+1){
                                    CollegeItemRank[j][1].sum=CollegeItemRank[j][1].sum+players[i][5].points;
                                }
                            }
                        }
                    }
                    for(int i=0;i<schoolnum;i++){
                        for(int j=i+1;j<schoolnum;j++){
                            if(CollegeItemRank[j][1].sum>CollegeItemRank[i][1].sum){
                                String newCIRname = CollegeItemRank[j][0].name;
                                CollegeItemRank[j][0].name = CollegeItemRank[i][0].name;
                                CollegeItemRank[i][0].name = newCIRname;

                                int newCIRpoint = CollegeItemRank[j][1].sum;
                                CollegeItemRank[j][1].sum = CollegeItemRank[i][1].sum;
                                CollegeItemRank[i][1].sum = newCIRpoint;
                            }
                        }
                    }
                    for(int i=0;i<schoolnum;i++){
                        for(int j=0;j<2;){
                            System.out.println(CollegeItemRank[i][j].name);
                            j++;
                            System.out.println(CollegeItemRank[i][j].sum);
                            j++;
                        }
                    }
                    break;
            }
        }
    }
}


