import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Graph {
    public List<String> verticesSize; //顶点个数
    public int[][] edges;//邻接矩阵
    public boolean[] Visit;//遍历；
    public Graph(int x){

        edges = new int[x][x];
        verticesSize = new ArrayList<>(x);
        Visit = new boolean[x];//初始化邻接矩阵

        for(int i=0;i<x;i++){
            Visit[i]=false;
        }
        for(int j=0;j<x;j++){
            verticesSize.add(j,""+j);
        }
        int a,b;
        for(a=0;a<x;a++){
            for(b=0;b<x;b++){
                edges[a][b]=0;
            }
        }
    }

    public void displayGraph(){
        for(int [] link:edges){
            System.out.println(Arrays.toString(link));
        }
    }

    public void AddEdgeWeight(int x1,int x2,int w){
        edges[x1][x2]=w;
    }

    private int getFirst(int a) {
        for(int i=0;i<verticesSize.size();i++){
            if(edges[a][i]>0&&!Visit[i]){
                return i;
            }
        }
        return 0;
    }

    public void bfs(){
        bfs(0);
    }

    public void dfs(){
        for(int i = 0;i<verticesSize.size();i++){
            if(Visit[i]){
                continue;
            }else {
                dfs(i);
            }
        }
    }


    private void bfs(int i) {
        LinkedList<Integer> list = new LinkedList<Integer>();
        System.out.println(i+"--");
        list.addLast(i);
        boolean ture = false;
        Visit[i] = ture;//i结点遍历并记录
        while(!list.isEmpty()){
            int a = list.removeFirst();
            int b = getFirst(a);
            while(b!=-1){
                list.addLast(b);
                System.out.println(b+"--");
                Visit[b]=true;
                b=getFirst(a);
            }
        }
    }

    private void dfs(int i) {
        System.out.println(i+"---");
        Visit[i]=true;
        int n=getFirst(i);
        if(n!=-1){
            dfs(n);
        }else {
            return;
        }
    }

    public void Topological(int n){
        int mos=0;
        int i;
        int reg;//reg为（n+1）mod顶点数
        int count = 0;//设置计数，跳出拓扑排序
        reg = (n+1)%verticesSize.size();
        for(i=0;i<verticesSize.size();i++){
            if(Visit[i]){
                count++;
            }
        }
        if(count == verticesSize.size()){
            System.out.println("out!");
        }
        else if(!Visit[n]){
            for(i=0;i<verticesSize.size();i++){
                if(edges[i][n]==1){
                    mos = 1;//判断是否有前缀
                    Topological(reg);
                }
            }
            if(mos==0){//无前驱
                System.out.println(n+"-->");
                Visit[n]=true;
                for(i=0;i<verticesSize.size();i++){
                    edges[i][n]=0;
                    edges[n][i]=0;
                }
                Topological(reg);
            }
            System.out.println();
        }
        else {
            Topological(reg);
        }
    }

    public void Dijkstra(){
        int x,y,z,min=100;
        int road;
        y=0;
        do{
            if(y==verticesSize.size()-1){
                road = edges[0][y];
                System.out.println("Dijkstra min :"+road);
                y=100;
            }
            else {
                for(x=1;x<verticesSize.size();x++){
                    if((edges[0][x] !=0)&&(edges[0][x]<min)){
                        min = edges[0][x];
                        y=x;
                    }
                }
                min = 100;
                for(z = y;z<verticesSize.size();z++){
                    if((edges[y][z] !=0)&&(edges[y][z]<min)){
                        edges[0][z]=edges[0][y]+edges[y][z];
                        edges[0][y]=0;
                        edges[y][z]=0;
                    }
                }
            }
        }while (y<verticesSize.size());
    }

}
