import java.util.Iterator;

public class BinaryTreeTest {
    public static void main(String[] args) {
        LinkedBinaryTree num1 = new LinkedBinaryTree("2");
        LinkedBinaryTree num2 = new LinkedBinaryTree("0");
        LinkedBinaryTree num3 = new LinkedBinaryTree("2");
        LinkedBinaryTree num4 = new LinkedBinaryTree("3", num1, num3);
        LinkedBinaryTree num5 = new LinkedBinaryTree("1", num2, num4);
        LinkedBinaryTree num6 = new LinkedBinaryTree("7", num4, num5);

        Iterator it;
        System.out.println("right of 1： ");
        System.out.println(num5.getRight());
        System.out.println("Contains 2? ");
        System.out.println(num1.contains("2"));

        System.out.println("PreOrder：  ");
        num6.toPreString();

        System.out.println();

        System.out.println("PostOrder： ");
        num6.toPostString();


        System.out.println(num6.toString());

    }
}