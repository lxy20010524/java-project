package search;

public class Searching {
    public static Comparable linearSearch(Comparable[] data,Comparable target){
        Comparable result = null;
        int index = 0;
        while(result == null && index < data.length){
            if(data[index].compareTo(target)==0)
                result = data[index];
            index++;
        }
        return result;
    }
}
