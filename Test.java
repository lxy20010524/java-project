import  java.util.Scanner;
public class Test {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Graph graph = null;
        UnGraph unGraph = null;
        int cos,use,x=0;
        do{
            System.out.println("Choose UnGraph(1) or Graph(2)");
            cos = scanner.nextInt();
            if(cos==1){
                System.out.println("input the number of the vertices");
                int num = scanner.nextInt();
                unGraph = new UnGraph(num);
                cos = 0;
                x=1;
            }
            else if(cos==2){
                System.out.println("input the number of the vertices");
                int num = scanner.nextInt();
                graph = new Graph(num);
                cos = 0;
                x=2;
            }
            else {
                cos = 2318;
            }
        }while (cos!=0);
        use=2318;
        int x1,x2,w;
        if(x==2){
            do{
                System.out.println("input x1,x2 and w");
                x1=scanner.nextInt();
                x2=scanner.nextInt();
                w=scanner.nextInt();
                graph.AddEdgeWeight(x1,x2,w);
                System.out.println("counter(1) or stop(0) ? ");
                use = scanner.nextInt();
            }while (use!=0);
            graph.displayGraph();

            System.out.println("bfs:");
            graph.bfs();

            System.out.println("dfs:");
            graph.dfs();

            System.out.println();

            System.out.println("Topological:");
            graph.Topological(0);

            System.out.println("Dijkstra");
            graph.Dijkstra();
        }
        else if(x==1){
            do{
                System.out.println("input x1,x2 and w");
                x1=scanner.nextInt();
                x2=scanner.nextInt();
                w=scanner.nextInt();
                unGraph.AddEdgeWeight(x1,x2,w);
                System.out.println("counter(1) or stop(0) ? ");
                use = scanner.nextInt();
            }while (use!=0);
            unGraph.displayGraph();
            unGraph.Prim(0);
        }

    }
}
