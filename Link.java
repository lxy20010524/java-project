public class Link {
    Node head = null;
    int nLiuXingYu = 0;

    public void AddNode(int d)
    {
        Node node = new Node(d);
        if (head == null) {
            head = node;
            nLiuXingYu++;
            return;
        }

        Node temp = head;

        while (temp.next != null)
        {
            temp = temp.next;
        }
        temp.next = node;
        nLiuXingYu++;
    }

    public void InsertNode(int index1,int d)
    {
        if (index1 > nLiuXingYu||index1 < 0)
        {
            System.out.println("超出范围！");
            return;
        }

        int index = index1 - 1;
        Node node = new Node(d);

        Node temp = head;
        int i;
        for(i = 0;i < index-1;i++)
        {
            temp = temp.next;
        }
        node.next = temp.next;
        temp.next = node;
        nLiuXingYu++;
    }

    public boolean DeleteNode(int index)
    {
        if (index < 1 || index > Length())
        {
            System.out.println("超出范围！");
            return false;
        }
        if (index == 1)
        {
            head = head.next;
            nLiuXingYu--;
            return true;
        }
        int i = 1;
        Node preNode = head;
        Node curNode = preNode.next;
        while (curNode != null)
        {
            if (i == index-1)
            {
                preNode.next = curNode.next;
                nLiuXingYu--;
                return true;
            }
            preNode = curNode;
            curNode = curNode.next;
            i++;
        }
        return false;
    }

    public int Length()
    {
        return nLiuXingYu;
    }

    public boolean DeleteNode1(Node n)
    {
        if (n == null || n.next == null)
        {
            return false;
        }
        int tmp = n.data;
        n.data = n.next.data;
        n.next.data = tmp;
        n.next = n.next.next;
        System.out.println("删除成功！");
        return true;
    }

    public void Print()
    {
        Node temp1 = head;
        while (temp1 != null)
        {
            System.out.print(temp1.data+" ");
            temp1 = temp1.next;
        }
        System.out.println();
    }

    public void Sort()
    {
        Node newHead = head;
        int temp;
        Node node = head;
        while (node != null) {
            Node reNode = node.next;
            while (reNode != null) {
                if (reNode.data < node.data) {
                    temp = reNode.data;
                    reNode.data = node.data;
                    node.data = temp;
                }
                reNode = reNode.next;
            }
            node = node.next;
        }
    }
}
