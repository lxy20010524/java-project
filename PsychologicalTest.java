import java.util.Scanner;

public class PsychologicalTest
{
    private LinkedBinaryTree tree;

    public PsychologicalTest()
    {
        String e1 = "你是否认为赛马娘是一部完美的作品";
        String e2 = "我认为你没有深入欣赏《赛马娘》这部作品，你觉得是这样吗？";
        String e3 = "我很高兴你这么认为，《赛马娘》将会推出手游，你会尝试吗";
        String e4 = "我认为你执迷不悟，你是否依旧坚持自己的想法？";
        String e5 = "是的，《赛马娘》是一部具有深度的作品，你会选择再次仔细品析这部作品吗？";
        String e6 = "或许你不喜欢手游，但我十分推荐你尝试一下，你真的不试试吗？";
        String e7 = "你获得手游赠送的米裕，你是否要重新选择？";
        String e8 = "恭喜你悬崖勒马，欢迎加入赛马娘的大队伍中";
        String e9 = "你实在是不明是非，我与你已经无话可说了";
        String e10 = "你的觉悟还是不够啊，这样是无法成为合格的爱马仕的";
        String e11 = "希望你能温故而知新，早日成为合格的爱马仕";
        String e12 = "我希望你能从不同的角度欣赏这部作品，你现在还不能称得上是合格的爱马仕";
        String e13 = "很高兴你能参与尝试，祝你早日成为合格的爱马仕";
        String e14 = "哦吼，你也是哥哥人，抱抱";
        String e15 ="爬";


        LinkedBinaryTree<String> n2, n3, n4, n5, n6, n7, n8, n9,
                n10, n11, n12, n13,n14,n15;

        n8 = new LinkedBinaryTree<String>(e8);
        n9 = new LinkedBinaryTree<String>(e9);
        n4 = new LinkedBinaryTree<String>(e4, n8, n9);

        n10 = new LinkedBinaryTree<String>(e10);
        n11 = new LinkedBinaryTree<String>(e11);
        n5 = new LinkedBinaryTree<String>(e5, n10, n11);

        n12 = new LinkedBinaryTree<String>(e12);
        n13 = new LinkedBinaryTree<String>(e13);
        n6 = new LinkedBinaryTree<String>(e6, n12, n13);

        n14 = new LinkedBinaryTree<String>(e14);
        n15 = new LinkedBinaryTree<String>(e15);
        n7 = new LinkedBinaryTree<String>(e7,n14,n15);

        n2 = new LinkedBinaryTree<String>(e2, n4, n5);
        n3 = new LinkedBinaryTree<String>(e3, n6, n7);

        tree = new LinkedBinaryTree<String>(e1, n2, n3);
    }

    public void start()
    {
        Scanner scan = new Scanner(System.in);
        LinkedBinaryTree<String> current = tree;

        System.out.println ("让我们聊一聊，请回答Y or N");
        while (current.size() > 1)
        {
            System.out.println (current.getRootElement());
            if (scan.nextLine().equalsIgnoreCase("N"))
                current = current.getLeft();
            else
                current = current.getRight();
        }

        System.out.println (current.getRootElement());
    }

    public static void main(String[] args){
        PsychologicalTest test = new PsychologicalTest();
        test.start();

    }
}