import java.io.*;
import java.util.Scanner;
public class shiyan6Test2 {
    public static void main(String[] args) throws IOException
    {
        Link link = new Link();
        Scanner scan = new Scanner(System.in);
        int n,a,b;


        File file = new File("test.txt");
        if (!file.exists()){
            file.createNewFile();
        }
        OutputStream outputStream1 = new FileOutputStream(file);
        byte[] test1 = {'5','9'};
        outputStream1.write(test1);


        InputStream inputStream1 = new FileInputStream(file);
        a = (char) inputStream1.read()-48;
        b = (char) inputStream1.read()-48;

        System.out.print("输入数字，输入0结束：");
        for (;;)
        {
            n = scan.nextInt();
            if (n == 0)
            {
                break;
            }
            link.AddNode(n);
        }
        System.out.println("输入完毕");
        System.out.print("输入的链表为");
        link.Print();
        System.out.println("此时链表中的元素个数为"+link.Length());
        link.InsertNode(6,a);
        System.out.print("将数据a添加进链表中");
        link.Print();
        System.out.println("此时链表中的元素个数为"+link.Length());
        link.InsertNode(4,b);
        System.out.print("将数据b添加进链表中");
        link.Print();
        System.out.println("此时链表中的元素个数为"+link.Length());
        System.out.print("删除数据a");
        link.DeleteNode(2);
        link.Print();
        System.out.println("此时链表中的元素个数为"+link.Length());
        link.Sort();
        System.out.print("使用选择排序对链表中的元素进行排序");
        link.Print();
    }
}
