package search;

public class Contact implements Comparable {
    private String firstName, lastName, phone;
    public Contact(String first, String last, String telephone) {
        firstName = first;
        lastName = last;
        phone = telephone;
    }
    public String toString() {
        return lastName + ", " + firstName + ":  " + phone;
    }
    public int compareTo(Object other) {
        int result;
        result = phone.compareTo(((Contact) other).phone);
        return result;
    }
}