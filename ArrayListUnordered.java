public class ArrayListUnordered<T> extends ArrayList implements UnorderedListADT<T> {
    @Override
    public void addToFront(T element) {
        if (size() == list.length)
            expandCapacity();
        for (int i = rear; i > 0; i--)
            list[i] = list[i - 1];
        list[0] = element;
        rear++;
        modCount++;
    }

    @Override
    public void addToRear(T element) {
        if (size() == list.length)
            expandCapacity();
        list[rear] = element;
        rear++;
        modCount++;
    }

    @Override
    public void addAfter(T element, T target) {
        if (size() == list.length)
            expandCapacity();

        int scan = 0;

        //find the insertion point
        while (scan < rear && !target.equals(list[scan]))
            scan++;
        if (scan == rear)
            try {
                throw new ElementNotFoundException("UnorderedList");
            } catch (ElementNotFoundException e) {
                e.printStackTrace();
            }

        scan++;

        for (int shilt = rear; shilt > scan; shilt--)
            list[shilt] = list[shilt - 1];

        list[scan] = element;
        rear++;
        modCount++;
    }
}