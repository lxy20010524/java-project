import java.util.ArrayList;
//使用iterator遍历集合的同时对集合进行修改就会出现java.util.ConcurrentModificationException异常
import java.util.ConcurrentModificationException;
import java.util.Iterator;

public class ArrayIterator<T> extends ArrayList<T> implements Iterator<T> {

    int iteratorModCount;
    int current;

    public ArrayIterator() {
        iteratorModCount = modCount;
        current = 0;

    }

    public boolean hasNext() throws ConcurrentModificationException {
        return super.iterator().hasNext();
    }

    public T next() throws ConcurrentModificationException {
        return super.iterator().next();
    }

    public void remove() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }
}